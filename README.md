# 2019_assignment3_FantaApp

# Membri del gruppo
*  Stefano Zuccarella 816482
*  Matteo Paolella 816933
*  Andrea Malerba 817426

# Come eseguire l'app
Clonare il progetto e poi all'interno eseguire da bash:
```
mvn spring-boot:run
```
Dopodiché, per iniziare ad utilizzare l'applicazione si vada al seguente link:
```
http://localhost:8080/
```
Verrà mostrata la pagina principale dell'applicazione, in cui si possono vedere i fantallenatori presenti all'interno dell'applicazione.

Ora FantaApp è completamente disponibile per l'uso!
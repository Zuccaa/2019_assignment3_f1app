package assignment3.fantacalcio.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Check;

@Entity(name="Fantallenatore")
@Table(name = "fantallenatore")
@Check(constraints = "nome <> '' and nickname <> ''")
public class Fantallenatore extends Persona{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "fantallenatore_Id")
	private long id;
	
	@Size(min = 1)
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Size(min = 1)
	@Column(name = "nickname", nullable = false)
	private String nickname;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "fantallenatore", cascade = CascadeType.ALL)
	private Fantasquadra fantasquadra;
	
	public Fantallenatore() {
		super();
	}
	
	public Fantallenatore(String nome, String nickname) {
		this.nome = nome;
		this.nickname = nickname;
		this.fantasquadra = new Fantasquadra();
	}
	
	public Fantallenatore(String nome, String nickname, Fantasquadra fantasquadra) {
		this.nome = nome;
		this.nickname = nickname;
		this.fantasquadra = fantasquadra;
	}
	
	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public Fantasquadra getFantasquadra() {
		return fantasquadra;
	}
	
	public void setFantasquadra(Fantasquadra fantasquadra) {
		this.fantasquadra = fantasquadra;
	}

	@Override
	public String toString() {
		return "Fantallenatore [id=" + id + ", nome=" + nome + 
				", nickname=" + nickname + ", fantasquadra=" + fantasquadra + "]";
	}
	
}
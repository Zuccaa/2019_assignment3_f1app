package assignment3.fantacalcio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Check;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import assignment3.fantacalcio.utility.Ruolo;

@Entity(name="Fantasquadra")
@Table(name="fantasquadra")
@Check(constraints = "nome <> ''")
public class Fantasquadra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "Id")
	private long id;
	
	@Size(min = 1)
	@Column(name = "nome", nullable = false)
	private String nomeFantasquadra;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fantallenatore_Id", nullable = false, unique = true)
	private Fantallenatore fantallenatore;
	
	@Size(max = 25)
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "calciatore_fantasquadra", joinColumns = @JoinColumn(name = "fantasquadra_id"), 
			  inverseJoinColumns = @JoinColumn(name = "calciatore_id"), uniqueConstraints={
					   @UniqueConstraint(columnNames={"fantasquadra_id", "calciatore_id"})})
	private List<Calciatore> calciatore;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fantasquadra1", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Partita> partiteInCasa;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fantasquadra2", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Partita> partiteInTrasferta;
	
	public Fantasquadra() {};
	
	public Fantasquadra(String nomeFantasquadra, Fantallenatore fantallenatore) {
		this.nomeFantasquadra = nomeFantasquadra;
		this.fantallenatore = fantallenatore;
		calciatore = new ArrayList<>();
		partiteInCasa = new ArrayList<>();
		partiteInTrasferta = new ArrayList<>();
	}
	
	public long getId() {
		return id;
	}

	public String getNomeFantasquadra() {
		return nomeFantasquadra;
	}

	public void setNomeFantasquadra(String nomeFantasquadra) {
		this.nomeFantasquadra = nomeFantasquadra;
	}
	
	public void setFantallenatore(Fantallenatore fantallenatore) {
		this.fantallenatore = fantallenatore;
	}

	public Fantallenatore getFantallenatore() {
		return fantallenatore;
	}	
	
	public List<Partita> getPartiteInCasa() {
		return partiteInCasa;
	}
	
	public List<Calciatore> getCalciatore() {
		return calciatore;
	}

	public void setCalciatore(List<Calciatore> calciatore) {
		if (calciatore.size() <= 25)
			this.calciatore = calciatore;
	}
	
	public void addCalciatore(Calciatore calciatore) {
		if (this.calciatore.size() < 25)
			this.calciatore.add(calciatore);
	}
	
	public void removeCalciatore(Calciatore calciatore) {
		if (this.calciatore.contains(calciatore))
			this.calciatore.remove(calciatore);
	}

	public List<Partita> getPartiteInTrasferta() {
		return partiteInTrasferta;
	}
	
	public List<Calciatore> getCalciatoriByRuolo(Iterable<Calciatore> calciatore, Ruolo r) {
		List<Calciatore> calciatoriConRuoloSelezionato = new ArrayList<>();
		
		for(Calciatore c: calciatore) {
			if (c.getRuolo().equals(r)) {
				calciatoriConRuoloSelezionato.add(c);
			}
		}
		
		return calciatoriConRuoloSelezionato;
	}
	
	@Override
	public String toString() {
		return "Fantasquadra [id=" + id + ", nomeFantasquadra=" + nomeFantasquadra + "]";
	}
	
}
package assignment3.fantacalcio.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.annotations.Check;

@Entity(name = "Partita")
@Table(name = "partita")
@Check(constraints = "gol_squadra_1 >= 0 and gol_squadra_2 >= 0")
public class Partita {

	@EmbeddedId
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private PartitaKey id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@MapsId("fantasquadraId1")
	@JoinColumn(name = "fantasquadraId1")
	private Fantasquadra fantasquadra1;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@MapsId("fantasquadraId2")
	@JoinColumn(name = "fantasquadraId2")
	private Fantasquadra fantasquadra2;
	
	@PositiveOrZero
	@Column(name = "gol_squadra_1", nullable = false)
	private int golSquadra1;
	
	@PositiveOrZero
	@Column(name = "gol_squadra_2", nullable = false)
	private int golSquadra2;
	
	public Partita() {}

	public Partita(PartitaKey id, Fantasquadra fantasquadra1, Fantasquadra fantasquadra2, 
			int golSquadra1, int golSquadra2) {
		this.id = id;
		this.fantasquadra1 = fantasquadra1;
		this.fantasquadra2 = fantasquadra2;
		this.golSquadra1 = golSquadra1;
		this.golSquadra2 = golSquadra2;
	}

	public PartitaKey getId() {
		return id;
	}

	public Fantasquadra getFantasquadra1() {
		return fantasquadra1;
	}

	public void setFantasquadra1(Fantasquadra fantasquadra1) {
		this.fantasquadra1 = fantasquadra1;
	}

	public Fantasquadra getFantasquadra2() {
		return fantasquadra2;
	}

	public void setFantasquadra2(Fantasquadra fantasquadra2) {
		this.fantasquadra2 = fantasquadra2;
	}

	public int getGolSquadra1() {
		return golSquadra1;
	}

	public void setGolSquadra1(int golSquadra1) {
		if (golSquadra1 >= 0)
			this.golSquadra1 = golSquadra1;
	}

	public int getGolSquadra2() {
		return golSquadra2;
	}

	public void setGolSquadra2(int golSquadra2) {
		if (golSquadra2 >= 0)
			this.golSquadra2 = golSquadra2;
	}

	@Override
	public String toString() {
		return "\"" + fantasquadra1.getNomeFantasquadra() + "\" " + golSquadra1 + " - " + 
				golSquadra2 + " \"" + fantasquadra2.getNomeFantasquadra() + "\"";
	}	
	
	@PrePersist
	public void checkMoreConstraints() throws Exception {
		if (fantasquadra1.equals(fantasquadra2))
			throw new Exception();
		if (fantasquadra1.getCalciatore().size() < 25
				|| fantasquadra2.getCalciatore().size() < 25)
			throw new Exception();
	}
	
}
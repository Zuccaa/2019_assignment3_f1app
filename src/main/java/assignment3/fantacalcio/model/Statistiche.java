package assignment3.fantacalcio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.annotations.Check;

@Entity(name = "Statistiche")
@Table(name = "statistiche")
@Check(constraints = "Partite_Giocate >= 0 and Gol_Fatti >= 0 and Gol_Subiti >= 0 and "
		+ "Autogol >= 0 and Rigori_Parati >= 0 and Rigori_Sbagliati >= 0 "
		+ "and Ammonizioni >= 0 and Espulsioni >= 0 and Assist >= 0")
public class Statistiche {

	@Id
	private long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "Calciatore_id", nullable = false)
	private Calciatore calciatore;
	
	@Column(name = "Partite_Giocate", nullable = false)
	@PositiveOrZero
	private int nPartiteGiocate;
	
	@Column(name = "Gol_Fatti", nullable = false)
	@PositiveOrZero
	private int nGolFatti;
	
	@Column(name = "Gol_Subiti", nullable = false)
	@PositiveOrZero
	private int nGolSubiti;
	
	@Column(name = "Autogol", nullable = false)
	@PositiveOrZero
	private int nAutogol;
	
	@Column(name = "Rigori_Parati", nullable = false)
	@PositiveOrZero
	private int nRigoriParati;
	
	@Column(name = "Rigori_Sbagliati", nullable = false)
	@PositiveOrZero
	private int nRigoriSbagliati;
	
	@Column(name = "Ammonizioni", nullable = false)
	@PositiveOrZero
	private int nAmmonizioni;
	
	@Column(name = "Espulsioni", nullable = false)
	@PositiveOrZero
	private int nEspulsioni;
	
	@Column(name = "Assist", nullable = false)
	@PositiveOrZero
	private int nAssist;
	
	protected Statistiche() {}
	
	public Statistiche(int nPartiteGiocate, int nGolFatti, int nGolSubiti, int nAutogol, int nRigoriParati,
			int nRigoriSbagliati, int nAmmonizioni, int nEspulsioni, int nAssist, Calciatore calciatore) {
		this.nPartiteGiocate = nPartiteGiocate;
		this.nGolFatti = nGolFatti;
		this.nGolSubiti = nGolSubiti;
		this.nAutogol = nAutogol;
		this.nRigoriParati = nRigoriParati;
		this.nRigoriSbagliati = nRigoriSbagliati;
		this.nAmmonizioni = nAmmonizioni;
		this.nEspulsioni = nEspulsioni;
		this.nAssist = nAssist;
		this.calciatore = calciatore;
	}
	
	public long getId() {
		return id;
	}
	
	public Calciatore getCalciatore() {
		return calciatore;
	}

	public int getnPartiteGiocate() {
		return nPartiteGiocate;
	}
	
	public void setnPartiteGiocate(int nPartiteGiocate) {
		this.nPartiteGiocate = nPartiteGiocate;
	}
	
	public int getnGolFatti() {
		return nGolFatti;
	}
	
	public void setnGolFatti(int nGolFatti) {
		this.nGolFatti = nGolFatti;
	}
	
	public int getnGolSubiti() {
		return nGolSubiti;
	}
	
	public void setnGolSubiti(int nGolSubiti) {
		this.nGolSubiti = nGolSubiti;
	}
	
	public int getnAutogol() {
		return nAutogol;
	}
	
	public void setnAutogol(int nAutogol) {
		this.nAutogol = nAutogol;
	}
	
	public int getnRigoriParati() {
		return nRigoriParati;
	}
	
	public void setnRigoriParati(int nRigoriParati) {
		this.nRigoriParati = nRigoriParati;
	}
	
	public int getnRigoriSbagliati() {
		return nRigoriSbagliati;
	}
	
	public void setnRigoriSbagliati(int nRigoriSbagliati) {
		this.nRigoriSbagliati = nRigoriSbagliati;
	}
	
	public int getnAmmonizioni() {
		return nAmmonizioni;
	}
	
	public void setnAmmonizioni(int nAmmonizioni) {
		this.nAmmonizioni = nAmmonizioni;
	}
	
	public int getnEspulsioni() {
		return nEspulsioni;
	}
	
	public void setnEspulsioni(int nEspulsioni) {
		this.nEspulsioni = nEspulsioni;
	}
	
	public int getnAssist() {
		return nAssist;
	}
	
	public void setnAssist(int nAssist) {
		this.nAssist = nAssist;
	}

	@Override
	public String toString() {
		return "Statistiche [nPartiteGiocate=" + nPartiteGiocate + ", nGolFatti=" + nGolFatti + ", nGolSubiti="
				+ nGolSubiti + ", nAutogol=" + nAutogol + ", nRigoriParati=" + nRigoriParati + ", nRigoriSbagliati="
				+ nRigoriSbagliati + ", nAmmonizioni=" + nAmmonizioni + ", nEspulsioni=" + nEspulsioni + ", nAssist="
				+ nAssist + "]";
	}
	
}
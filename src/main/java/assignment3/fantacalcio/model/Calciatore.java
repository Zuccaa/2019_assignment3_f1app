package assignment3.fantacalcio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Check;

import assignment3.fantacalcio.utility.Ruolo;

@Entity(name = "Calciatore")
@Table(name = "calciatore")
@Check(constraints = "nome <> '' and squadra <> ''")
public class Calciatore extends Persona {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Size(min = 1)
	@Column(name = "nome", unique=true, nullable = false)
	private String nome;
	
	@Size(min = 1)
	@Column(name = "squadra", nullable = false)
	private String squadra;
	
	@Enumerated(EnumType.STRING)
	@Pattern(regexp = "P|D|C|A")
	@Column(name = "ruolo")
	private Ruolo ruolo;
	
	@ManyToMany(mappedBy = "calciatore")
	private List<Fantasquadra> fantasquadra;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "calciatore", cascade = CascadeType.ALL, orphanRemoval = true)
	private Statistiche statistiche;

	public Calciatore() {}
	
	public Calciatore(String nome, String squadra, Ruolo ruolo) {
		this.nome = nome;
		this.squadra = squadra;
		this.ruolo = ruolo;
		fantasquadra = new ArrayList<>();
	}

	public List<Fantasquadra> getFantasquadra() {
		return fantasquadra;
	}
	
	public void setFantasquadra(List<Fantasquadra> fantasquadra) {
		this.fantasquadra = fantasquadra;
	}
	
	public void addFantasquadra(Fantasquadra fantasquadra) {
		this.fantasquadra.add(fantasquadra);
	}
	
	public void removeFantasquadra(Fantasquadra fantasquadra) {
		if (this.fantasquadra.contains(fantasquadra))
			this.fantasquadra.remove(fantasquadra);
	}
	
	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getSquadra() {
		return squadra;
	}

	public Ruolo getRuolo() {
		return ruolo;
	}

	@Override
	public String toString() {
		return nome + " (" + squadra + ", " + ruolo + ")";
	}

}

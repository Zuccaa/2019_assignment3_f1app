package assignment3.fantacalcio.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PartitaKey implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "fantasquadra_id_1")
	private long fantasquadraId1;
	
	@Column(name = "fantasquadra_id_2")
	private long fantasquadraId2;
	
	protected PartitaKey() {}
	
	public PartitaKey(long fantasquadraId1, long fantasquadraId2) {
		this.fantasquadraId1 = fantasquadraId1;
		this.fantasquadraId2 = fantasquadraId2;
	}

	public Long getFantasquadraId1() {
		return fantasquadraId1;
	}

	public void setFantasquadraId1(long fantasquadraId1) {
		this.fantasquadraId1 = fantasquadraId1;
	}

	public Long getFantasquadraId2() {
		return fantasquadraId2;
	}

	public void setFantasquadraId2(long fantasquadraId2) {
		this.fantasquadraId2 = fantasquadraId2;
	}
	
}
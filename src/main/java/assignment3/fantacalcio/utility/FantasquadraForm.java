package assignment3.fantacalcio.utility;

import assignment3.fantacalcio.model.Fantasquadra;

public class FantasquadraForm {

	private Fantasquadra fantasquadra;
	
	public FantasquadraForm() {}

	public Fantasquadra getFantasquadra() {
		return fantasquadra;
	}

	public void setFantasquadra(Fantasquadra fantasquadra) {
		this.fantasquadra = fantasquadra;
	}
	
	
}

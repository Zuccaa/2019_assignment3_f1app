package assignment3.fantacalcio.utility;

import assignment3.fantacalcio.model.Calciatore;

public class CalciatoreForm {

	private Calciatore calciatore;
	
	public CalciatoreForm() {}

	public Calciatore getCalciatore() {
		return calciatore;
	}

	public void setCalciatore(Calciatore calciatore) {
		this.calciatore = calciatore;
	}
	
	
}

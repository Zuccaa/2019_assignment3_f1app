package assignment3.fantacalcio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="STATISTICHE NON PRESENTI")
public class StatisticheNonTrovateException extends RuntimeException {

	private static final long serialVersionUID = -3249155015348911773L;

	public StatisticheNonTrovateException() {
		super();
	}

	public StatisticheNonTrovateException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public StatisticheNonTrovateException(String message) {
		super(message);
	}
	
	public StatisticheNonTrovateException(Throwable cause) {
		super(cause);
	}
	
}
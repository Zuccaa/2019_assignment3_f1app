package assignment3.fantacalcio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="CALCIATORE NON AGGIUNGIBILE ALLA FANTASQUADRA")
public class CalciatoreNonAggiungibileException extends RuntimeException {

	private static final long serialVersionUID = 2352634344537340107L;

	public CalciatoreNonAggiungibileException() {
		super();
	}

	public CalciatoreNonAggiungibileException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CalciatoreNonAggiungibileException(String message) {
		super(message);
	}
	
	public CalciatoreNonAggiungibileException(Throwable cause) {
		super(cause);
	}
	
}
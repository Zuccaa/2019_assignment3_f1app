package assignment3.fantacalcio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="FANTASQUADRA NON PRESENTE")
public class FantasquadraNonTrovataException extends RuntimeException {

	private static final long serialVersionUID = -3183902194127704025L;

	public FantasquadraNonTrovataException() {
		super();
	}

	public FantasquadraNonTrovataException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public FantasquadraNonTrovataException(String message) {
		super(message);
	}
	
	public FantasquadraNonTrovataException(Throwable cause) {
		super(cause);
	}
	
}
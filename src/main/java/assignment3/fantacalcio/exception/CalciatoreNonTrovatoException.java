package assignment3.fantacalcio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="CALCIATORE NON PRESENTE")
public class CalciatoreNonTrovatoException extends RuntimeException {

	private static final long serialVersionUID = -1935591977192060187L;

	public CalciatoreNonTrovatoException() {
		super();
	}

	public CalciatoreNonTrovatoException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CalciatoreNonTrovatoException(String message) {
		super(message);
	}
	
	public CalciatoreNonTrovatoException(Throwable cause) {
		super(cause);
	}
	
}
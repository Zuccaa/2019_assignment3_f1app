package assignment3.fantacalcio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="FANTALLENATORE NON PRESENTE")
public class FantallenatoreNonTrovatoException extends RuntimeException {

	private static final long serialVersionUID = -6378034624233998029L;

	public FantallenatoreNonTrovatoException() {
		super();
	}

	public FantallenatoreNonTrovatoException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public FantallenatoreNonTrovatoException(String message) {
		super(message);
	}
	
	public FantallenatoreNonTrovatoException(Throwable cause) {
		super(cause);
	}
	
}
package assignment3.fantacalcio.repository;

import org.springframework.data.repository.CrudRepository;

import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.model.Partita;

public interface PartitaRepository extends CrudRepository<Partita, Long> {
	
	Iterable<Partita> findByFantasquadra1(Fantasquadra fs1);
	
	Iterable<Partita> findByFantasquadra2(Fantasquadra fs2);
	
	Iterable<Partita> findByFantasquadra1OrFantasquadra2(Fantasquadra fs1, Fantasquadra fs2);
	
}
package assignment3.fantacalcio.repository;

import org.springframework.data.repository.CrudRepository;

import assignment3.fantacalcio.model.Statistiche;

public interface StatisticheRepository extends CrudRepository<Statistiche, Long> {

}

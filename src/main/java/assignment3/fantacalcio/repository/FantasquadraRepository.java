package assignment3.fantacalcio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import assignment3.fantacalcio.model.Fantasquadra;

public interface FantasquadraRepository extends CrudRepository<Fantasquadra, Long> {
		
	List<Fantasquadra> findByIdNotIn(List<Long> id);
		
	@Transactional
	@Query(value = "select * from Fantasquadra where fantallenatore_id = ?1", nativeQuery = true)
	Optional<Fantasquadra> findByFantallenatoreId(long id);
	
	@Transactional
	@Modifying
	@Query(value = "delete from Fantasquadra where id = ?1")
	void deleteById(long id);
	
}
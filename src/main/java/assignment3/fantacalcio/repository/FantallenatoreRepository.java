package assignment3.fantacalcio.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import assignment3.fantacalcio.model.Fantallenatore;

public interface FantallenatoreRepository extends CrudRepository<Fantallenatore, Long> {
	
	@Transactional
	@Modifying
	@Query (value = "delete from Fantallenatore where id = ?1")
	void deleteById(long id);

}
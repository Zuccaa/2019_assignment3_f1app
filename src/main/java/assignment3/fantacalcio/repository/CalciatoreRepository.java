package assignment3.fantacalcio.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import assignment3.fantacalcio.model.Calciatore;
import assignment3.fantacalcio.model.Fantasquadra;

public interface CalciatoreRepository extends CrudRepository<Calciatore, Long> {
						
	Iterable<Calciatore> findByIdNotInOrderByNome(List<Long> id);
	
	Iterable<Calciatore> findByFantasquadra(Fantasquadra fantasquadra);
		
	Iterable<Calciatore> findAllByOrderByNome();
	
}
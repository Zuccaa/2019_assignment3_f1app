package assignment3.fantacalcio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import assignment3.fantacalcio.exception.CalciatoreNonAggiungibileException;
import assignment3.fantacalcio.exception.FantallenatoreNonTrovatoException;
import assignment3.fantacalcio.exception.FantasquadraNonTrovataException;
import assignment3.fantacalcio.model.Calciatore;
import assignment3.fantacalcio.model.Fantallenatore;
import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.model.Partita;
import assignment3.fantacalcio.repository.CalciatoreRepository;
import assignment3.fantacalcio.repository.FantallenatoreRepository;
import assignment3.fantacalcio.repository.FantasquadraRepository;
import assignment3.fantacalcio.repository.PartitaRepository;
import assignment3.fantacalcio.utility.CalciatoreForm;
import assignment3.fantacalcio.utility.Ruolo;

@Controller
public class FantasquadraController {
	
	@Autowired
	private FantasquadraRepository fsRepository;
	
	@Autowired
	private FantallenatoreRepository faRepository;
			
	@Autowired
	private PartitaRepository pRepository;
	
	@Autowired
	private CalciatoreRepository cRepository;
		
	@GetMapping("/fantasquadra/{id}")
	public ModelAndView fantasquadra(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantallenatore> fantallenatore = faRepository.findById(id);
		fantallenatore.orElseThrow(FantallenatoreNonTrovatoException::new);
		
		Optional<Fantasquadra> team = fsRepository.findByFantallenatoreId(id);
				
		if(!team.isPresent()){
			mv.addObject("idAllenatore", id);
			mv.addObject("fantasquadra", new Fantasquadra());
			mv.setViewName("fantasquadra_new");
			return mv;
		}
		
		Iterable<Calciatore> calciatoriInFantasquadra = cRepository.findByFantasquadra(team.get());
		
		Iterable<Partita> partiteGiocate = pRepository.findByFantasquadra1OrFantasquadra2(team.get(), team.get());

		mv.addObject("fantasquadra", team.get());
		mv.addObject("calciatoriInFantasquadra", calciatoriInFantasquadra);
		mv.addObject("partiteGiocate", partiteGiocate);
		mv.setViewName("fantasquadra");

		return mv;
	}
	
	@GetMapping("/modifyNomeFantasquadra/{id}")
	public ModelAndView modifyNomeFantasquadra(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);

		team.orElseThrow(FantasquadraNonTrovataException::new);
		
		mv.addObject("fantasquadra", team.get());
		mv.addObject("nomeFantasquadra", team.get().getNomeFantasquadra());
		mv.setViewName("modifyNomeFantasquadra");
		
		return mv;
	}
	
	@PostMapping("/modifyNomeFantasquadra/{id}")
	public ModelAndView nomeFantasquadraModified(@PathVariable("id") long id, @ModelAttribute Fantasquadra fantasquadra) {
		ModelAndView mv = new ModelAndView();
		
		String nomeFantasquadra = fantasquadra.getNomeFantasquadra();
		Fantasquadra fs = fsRepository.findById(id).get();
		fs.setNomeFantasquadra(nomeFantasquadra);
		fsRepository.save(fs);

		mv.addObject("fantasquadra", fs);
		mv.setViewName("nomeFantasquadraModified");

		return mv;
	}
	
	@GetMapping("/deleteFantasquadra/{id}")
	public ModelAndView deleteFantasquadra(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);

		mv.addObject("fantasquadra", team.get());
		mv.setViewName("deleteFantasquadra");
		
		return mv;
	}
	
	@PostMapping("/deleteFantasquadra/{id}")
	public ModelAndView fantasquadraDeleted(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantasquadra> fantasquadraDaRimuovere = fsRepository.findById(id);
		
		fantasquadraDaRimuovere.orElseThrow(FantasquadraNonTrovataException::new);

		fsRepository.deleteById(fantasquadraDaRimuovere.get().getId());
		
		mv.addObject("fantasquadra", fantasquadraDaRimuovere.get());
		mv.setViewName("fantasquadraDeleted");
		
		return mv;
	}
	
	@GetMapping("/modifyCalciatoriInFantasquadra/{id}")
	public ModelAndView modifyCalciatoriInFantasquadra(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);

		Iterable<Calciatore> c = cRepository.findByFantasquadra(team.get());
		List<Calciatore> portieriInFantasquadra = team.get().getCalciatoriByRuolo(c, Ruolo.P);
		List<Calciatore> difensoriInFantasquadra = team.get().getCalciatoriByRuolo(c, Ruolo.D);
		List<Calciatore> centrocampistiInFantasquadra = team.get().getCalciatoriByRuolo(c, Ruolo.C);
		List<Calciatore> attaccantiInFantasquadra = team.get().getCalciatoriByRuolo(c, Ruolo.A);

		mv.addObject("fantasquadra", team.get());
		mv.addObject("portieriInFantasquadra", portieriInFantasquadra);
		mv.addObject("difensoriInFantasquadra", difensoriInFantasquadra);
		mv.addObject("centrocampistiInFantasquadra", centrocampistiInFantasquadra);
		mv.addObject("attaccantiInFantasquadra", attaccantiInFantasquadra);

		mv.setViewName("modifyCalciatoriInFantasquadra");
		
		return mv;
	}
	
	@GetMapping("/addCalciatoriInFantasquadra/{id}")
	public ModelAndView addCalciatoriInFantasquadra(@PathVariable("id") long id) {	
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);

		Iterable<Calciatore> calciatoriInFantasquadra = cRepository.findByFantasquadra(team.get());
			
		int nPortieriInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.P).size();
		int nDifensoriInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.D).size();
		int nCentrocampistiInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.C).size();
		int nAttaccantiInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.A).size();
			
		List<Calciatore> portieriDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.P);
		List<Calciatore> difensoriDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.D);
		List<Calciatore> centrocampistiDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.C);
		List<Calciatore> attaccantiDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.A);

		mv.addObject("calciatoreForm", new CalciatoreForm());
		mv.addObject("fantasquadra", team.get());
		mv.addObject("nPortieriInFantasquadra", nPortieriInFantasquadra);
		mv.addObject("nDifensoriInFantasquadra", nDifensoriInFantasquadra);
		mv.addObject("nCentrocampistiInFantasquadra", nCentrocampistiInFantasquadra);
		mv.addObject("nAttaccantiInFantasquadra", nAttaccantiInFantasquadra);
		mv.addObject("portieriDisponibili", portieriDisponibili);
		mv.addObject("difensoriDisponibili", difensoriDisponibili);
		mv.addObject("centrocampistiDisponibili", centrocampistiDisponibili);
		mv.addObject("attaccantiDisponibili", attaccantiDisponibili);
		mv.setViewName("addCalciatoriInFantasquadra");
		
		return mv;
	}
	
	@PostMapping("/addCalciatoriInFantasquadra/{id}")
	public ModelAndView calciatoreAddedInFantasquadra(@PathVariable("id") long id,
			@ModelAttribute Calciatore calciatore) throws Exception {	
		ModelAndView mv = new ModelAndView();

		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		Iterable<Calciatore> calciatoriInFantasquadra = cRepository.findByFantasquadra(team.get());

		team.orElseThrow(FantasquadraNonTrovataException::new);

		switch (calciatore.getRuolo()) {
			case P:
				if (team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.P).size() < 3)
					team.get().addCalciatore(calciatore);
				else
					throw new CalciatoreNonAggiungibileException();
				break;
			case D:
				if (team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.D).size() < 8)
					team.get().addCalciatore(calciatore);
				else
					throw new CalciatoreNonAggiungibileException();
				break;
			case C:
				if (team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.C).size() < 8)
					team.get().addCalciatore(calciatore);
				else
					throw new CalciatoreNonAggiungibileException();
				break;
			case A:
				if (team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.A).size() < 6)
					team.get().addCalciatore(calciatore);
				else
					throw new CalciatoreNonAggiungibileException();
				break;
		}
		
		fsRepository.save(team.get());
						
		mv.addObject("calciatoreInFantasquadra", calciatore);
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("calciatoreAdded");
		
		return mv;
	}
	
	@PostMapping("/addCalciatoriRandomInFantasquadra/{id}")
	public ModelAndView addCalciatoriRandomInFantasquadra(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);

		Iterable<Calciatore> calciatoriInFantasquadra = cRepository.findByFantasquadra(team.get());
			
		int nPortieriInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.P).size();
		int nDifensoriInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.D).size();
		int nCentrocampistiInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.C).size();
		int nAttaccantiInFantasquadra = team.get().getCalciatoriByRuolo(calciatoriInFantasquadra, Ruolo.A).size();
			
		List<Calciatore> portieriDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.P);
		List<Calciatore> difensoriDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.D);
		List<Calciatore> centrocampistiDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.C);
		List<Calciatore> attaccantiDisponibili = findCalciatoriDisponibiliPerRuolo(team.get(), Ruolo.A);
			
		addGiocatoriRandomInFantasquadra(team.get(), portieriDisponibili, nPortieriInFantasquadra, 3);
		addGiocatoriRandomInFantasquadra(team.get(), difensoriDisponibili, nDifensoriInFantasquadra, 8);
		addGiocatoriRandomInFantasquadra(team.get(), centrocampistiDisponibili, nCentrocampistiInFantasquadra, 8);
		addGiocatoriRandomInFantasquadra(team.get(), attaccantiDisponibili, nAttaccantiInFantasquadra, 6);
			
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("calciatoriRandomAdded");
		
		return mv;
	}
	
	@GetMapping("/removeCalciatoriFromFantasquadra/{id}")
	public ModelAndView removeCalciatoriFromFantasquadra(@PathVariable("id") long id) {	
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);

		Iterable<Calciatore> calciatoriDisponibili = cRepository.findByFantasquadra(team.get());
			
		mv.addObject("calciatoreForm", new CalciatoreForm());
		mv.addObject("fantasquadra", team.get());
		mv.addObject("calciatoriDisponibili", calciatoriDisponibili);
		mv.setViewName("deleteCalciatoriInFantasquadra");
		
		return mv;
	}
	
	@PostMapping("/removeCalciatoriFromFantasquadra/{id}")
	public ModelAndView calciatoreRemovedFromFantasquadra(@PathVariable("id") long id,
			@ModelAttribute Calciatore calciatore) {	
		ModelAndView mv = new ModelAndView();

		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
			
		team.get().removeCalciatore(calciatore);
		fsRepository.save(team.get());
			
		mv.addObject("calciatoreInFantasquadra", calciatore);
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("calciatoreRemoved");
		
		return mv;
	}
	
	public List<Calciatore> findCalciatoriDisponibiliPerRuolo(Fantasquadra fantasquadra, Ruolo ruolo) {
		List<Calciatore> calciatoriDisponibiliPerRuolo = new ArrayList<>();
		List<Long> idCalciatoriNonDisponibili = new ArrayList<>();
		Iterable<Calciatore> calciatoriInFantasquadra = cRepository.findByFantasquadra(fantasquadra);
		
		for (Calciatore c: calciatoriInFantasquadra)
			idCalciatoriNonDisponibili.add(c.getId());
		
		Iterable<Calciatore> calciatoriDisponibili = cRepository.findByIdNotInOrderByNome(idCalciatoriNonDisponibili);
		
		if (idCalciatoriNonDisponibili.size() <= 0) 
			calciatoriDisponibili = cRepository.findAllByOrderByNome();
		
		for (Calciatore c: calciatoriDisponibili)
			if (c.getRuolo() == ruolo)
				calciatoriDisponibiliPerRuolo.add(c);
		
		return calciatoriDisponibiliPerRuolo;
	}
	
	public void addGiocatoriRandomInFantasquadra(Fantasquadra fantasquadra, List<Calciatore> calciatoriDisponibili, 
			int nCalciatoriInFantasquadra, int nGiocatoriInseribili) {
		Random r = new Random();
		Calciatore calciatoreDaAggiungere;
		while (nCalciatoriInFantasquadra < nGiocatoriInseribili) {
			calciatoreDaAggiungere = calciatoriDisponibili.get(r.nextInt(calciatoriDisponibili.size()));
			calciatoriDisponibili.remove(calciatoreDaAggiungere);
			fantasquadra.addCalciatore(calciatoreDaAggiungere);
			fsRepository.save(fantasquadra);
			nCalciatoriInFantasquadra++;
		}
	}
	
}
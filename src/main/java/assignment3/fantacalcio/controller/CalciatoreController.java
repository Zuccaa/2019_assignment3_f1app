package assignment3.fantacalcio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import assignment3.fantacalcio.exception.CalciatoreNonTrovatoException;
import assignment3.fantacalcio.exception.FantasquadraNonTrovataException;
import assignment3.fantacalcio.model.Calciatore;
import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.repository.CalciatoreRepository;
import assignment3.fantacalcio.repository.FantasquadraRepository;

@Controller
public class CalciatoreController {
	
	@Autowired
	private CalciatoreRepository cRepository;
	
	@Autowired
	private FantasquadraRepository fsRepository;
	  
	@GetMapping("/fantasquadra/{idFantasquadra}/calciatore/{idCalciatore}")
	public ModelAndView calciatore(@PathVariable("idFantasquadra") long idFantasquadra, 
			@PathVariable("idCalciatore") long idCalciatore) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(idFantasquadra);
		Optional<Calciatore> calciatore = cRepository.findById(idCalciatore);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		calciatore.orElseThrow(CalciatoreNonTrovatoException::new);
		
		mv.addObject("calciatore", calciatore.get());
		mv.addObject("squadra", calciatore.get().getSquadra());
		mv.addObject("ruolo", calciatore.get().getRuolo());
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("calciatore");

		return mv;
	}
	
}
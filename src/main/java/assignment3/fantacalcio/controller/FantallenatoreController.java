package assignment3.fantacalcio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import assignment3.fantacalcio.exception.FantallenatoreNonTrovatoException;
import assignment3.fantacalcio.model.Fantallenatore;
import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.repository.FantallenatoreRepository;
import assignment3.fantacalcio.repository.FantasquadraRepository;

@Controller
public class FantallenatoreController {

	@Autowired
	private FantallenatoreRepository faRepository;
	
	@Autowired
	private FantasquadraRepository fsRepository;
		
	@GetMapping("/")
	public ModelAndView fantallenatori() {
		ModelAndView mv = new ModelAndView();

		Iterable<Fantallenatore> users = faRepository.findAll();

		mv.addObject("fantallenatori", users);
		mv.setViewName("fantallenatori");

		return mv;
	}
	
	@GetMapping("/fantallenatori/{id}")
	public ModelAndView fantallenatore(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);
		
		mv.addObject("fantallenatore", user.get());
		mv.setViewName("fantallenatore");

		return mv;
	}
	
	@GetMapping("/modifyNomeFantallenatore/{id}")
	public ModelAndView modifyNomeFantallenatore(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);
		
		mv.addObject("fantallenatore", user.get());
		mv.addObject("nomeFantallenatore", user.get().getNome());
		mv.setViewName("modifyNomeFantallenatore");

		return mv;
	}
	
	@PostMapping("/modifyNomeFantallenatore/{id}")
	public ModelAndView nomeFantallenatoreModified(@PathVariable("id") long id,
			@ModelAttribute Fantallenatore fantallenatore) {
		ModelAndView mv = new ModelAndView();

		String nome = fantallenatore.getNome();
		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);

		user.get().setNome(nome);
		faRepository.save(user.get());

		mv.addObject("fantallenatore", user.get());
		mv.setViewName("nomeFantallenatoreModified");
		
		return mv;
	}
	
	@GetMapping("/modifyNicknameFantallenatore/{id}")
	public ModelAndView modifyNicknameFantallenatore(@PathVariable("id") long id) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);
		
		mv.addObject("fantallenatore", user.get());
		mv.addObject("nicknameFantallenatore", user.get().getNickname());
		mv.setViewName("modifyNicknameFantallenatore");

		return mv;
	}
	
	@PostMapping("/modifyNicknameFantallenatore/{id}")
	public ModelAndView nicknameFantallenatoreModified(@PathVariable("id") long id,
			@ModelAttribute Fantallenatore fantallenatore) {
		ModelAndView mv = new ModelAndView();

		String nickname = fantallenatore.getNickname();
		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);
		
		user.get().setNickname(nickname);
		faRepository.save(user.get());

		mv.addObject("fantallenatore", user.get());
		mv.setViewName("nicknameFantallenatoreModified");

		return mv;
	}
	
	@GetMapping("/addfantallenatore")
	public String addfantallenatoreForm(Model model) {
		model.addAttribute("fantallenatore", new Fantallenatore());
		return "fantallenatore_new.html";
	}

	@PostMapping("/addfantallenatore")
	public String addfantallenatoreSubmit(@ModelAttribute Fantallenatore fa) {
		faRepository.save(fa);
		return "fantallenatore_created.html";
	}
	
	@GetMapping("/deleteFantallenatore/{id}")
	public ModelAndView deleteFantallenatore(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantallenatore> user = faRepository.findById(id);
		
		user.orElseThrow(FantallenatoreNonTrovatoException::new);

		mv.addObject("fantallenatore", user.get());
		mv.setViewName("deleteFantallenatore");
		
		return mv;
	}
	
	@PostMapping("/deleteFantallenatore/{id}")
	public ModelAndView fantallenatoreDeleted(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantallenatore> fantallenatoreDaRimuovere = faRepository.findById(id);
		Optional<Fantasquadra> fantasquadraDaRimuovere = fsRepository.findByFantallenatoreId(id);
		
		fantallenatoreDaRimuovere.orElseThrow(FantallenatoreNonTrovatoException::new);

		if(fantasquadraDaRimuovere.isPresent())
			fsRepository.deleteById(fantasquadraDaRimuovere.get().getId());
		
		faRepository.deleteById(id);
		
		mv.addObject("fantallenatore", fantallenatoreDaRimuovere.get());
		mv.setViewName("fantallenatoreDeleted");
		
		return mv;
	}
	
	@PostMapping("/fantasquadra_created/{id_fa}")
	public ModelAndView addfantasquadraSubmit(@ModelAttribute Fantasquadra fs, @PathVariable long id_fa) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantallenatore> fantallenatoreDaAggiungere = faRepository.findById(id_fa);

		fantallenatoreDaAggiungere.orElseThrow(FantallenatoreNonTrovatoException::new);

		fs.setFantallenatore(fantallenatoreDaAggiungere.get());
		fsRepository.save(fs);
		
		mv.addObject("fantasquadra", fs);
		mv.setViewName("fantasquadra_created");
		return mv;
	}

}
package assignment3.fantacalcio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import assignment3.fantacalcio.exception.FantasquadraNonTrovataException;
import assignment3.fantacalcio.exception.StatisticheNonTrovateException;
import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.model.Statistiche;
import assignment3.fantacalcio.repository.FantasquadraRepository;
import assignment3.fantacalcio.repository.StatisticheRepository;

@Controller
public class StatisticheController {
	
	@Autowired
	private StatisticheRepository sRepository;
	
	@Autowired
	private FantasquadraRepository fsRepository;
	  
	@GetMapping("/fantasquadra/{idFantasquadra}/statisticheCalciatore/{idStatistiche}")
	public ModelAndView statisticheCalciatore(@PathVariable("idFantasquadra") long idFantasquadra, 
			@PathVariable("idStatistiche") long idStatistiche) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(idFantasquadra);
		Optional<Statistiche> statistiche = sRepository.findById(idStatistiche);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		statistiche.orElseThrow(StatisticheNonTrovateException::new);

		mv.addObject("fantasquadra", team.get());
		mv.addObject("stats", statistiche.get());
		mv.addObject("nPartiteGiocate", statistiche.get().getnPartiteGiocate());
		mv.addObject("nGolFatti", statistiche.get().getnGolFatti());
		mv.addObject("nGolSubiti", statistiche.get().getnGolSubiti());
		mv.addObject("nAutogol", statistiche.get().getnAutogol());
		mv.addObject("nRigoriParati", statistiche.get().getnRigoriParati());
		mv.addObject("nRigoriSbagliati", statistiche.get().getnRigoriSbagliati());
		mv.addObject("nAmmonizioni", statistiche.get().getnAmmonizioni());
		mv.addObject("nEspulsioni", statistiche.get().getnEspulsioni());
		mv.addObject("nAssist", statistiche.get().getnAssist());
		mv.setViewName("statistiche");

		return mv;
	}
	
}
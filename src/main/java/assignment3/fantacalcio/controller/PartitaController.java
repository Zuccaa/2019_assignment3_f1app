package assignment3.fantacalcio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import assignment3.fantacalcio.exception.FantasquadraNonTrovataException;
import assignment3.fantacalcio.model.Fantasquadra;
import assignment3.fantacalcio.model.Partita;
import assignment3.fantacalcio.model.PartitaKey;
import assignment3.fantacalcio.repository.FantasquadraRepository;
import assignment3.fantacalcio.repository.PartitaRepository;
import assignment3.fantacalcio.utility.FantasquadraForm;

@Controller
public class PartitaController {
		
	@Autowired
	FantasquadraRepository fsRepository;
	
	@Autowired
	PartitaRepository pRepository;

	@GetMapping("/giocaInCasa/{id}")
	public ModelAndView giocaInCasa(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		
		List<Fantasquadra> fantasquadreAffrontabiliInCasa = getFantasquadreAffrontabili(team.get(), 1);

		mv.addObject("fantasquadra", team.get());
		mv.addObject("fantasquadraForm", new FantasquadraForm());
		mv.addObject("fantasquadreAffrontabiliInCasa", fantasquadreAffrontabiliInCasa);
		mv.setViewName("giocaPartitaInCasa");
		
		return mv;
	}
	
	@PostMapping("/giocaInCasa/{id}")
	public ModelAndView partitaInCasaGiocata(@PathVariable("id") long id,
			@ModelAttribute Fantasquadra fantasquadra) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		
		PartitaKey pk = new PartitaKey(id, fantasquadra.getId());
		Random golInCasa = new Random();
		Random golInTrasferta = new Random();
		Partita p = new Partita(pk, team.get(), fantasquadra, golInCasa.nextInt(7), golInTrasferta.nextInt(6));

		pRepository.save(p);
			
		mv.addObject("partita", p);
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("partitaGiocata");
		
		return mv;
	}

	@GetMapping("/giocaInTrasferta/{id}")
	public ModelAndView giocaInTrasferta(@PathVariable long id) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		
		List<Fantasquadra> fantasquadreAffrontabiliInTrasferta = getFantasquadreAffrontabili(team.get(), 2);

		mv.addObject("fantasquadra", team.get());
		mv.addObject("fantasquadraForm", new FantasquadraForm());
		mv.addObject("fantasquadreAffrontabiliInTrasferta", fantasquadreAffrontabiliInTrasferta);
		mv.setViewName("giocaPartitaInTrasferta");
		
		return mv;
	}
	
	@PostMapping("/giocaInTrasferta/{id}")
	public ModelAndView partitaInTrasfertaGiocata(@PathVariable("id") long id,
			@ModelAttribute Fantasquadra fantasquadra) {
		ModelAndView mv = new ModelAndView();

		Optional<Fantasquadra> team = fsRepository.findById(id);
		
		team.orElseThrow(FantasquadraNonTrovataException::new);
		
		PartitaKey pk = new PartitaKey(fantasquadra.getId(), id);
		Random golInCasa = new Random();
		Random golInTrasferta = new Random();
		Partita p = new Partita(pk, fantasquadra, team.get(), golInCasa.nextInt(7), golInTrasferta.nextInt(6));

		pRepository.save(p);
			
		mv.addObject("partita", p);
		mv.addObject("fantasquadra", team.get());
		mv.setViewName("partitaGiocata");
		
		return mv;
	}
	
	public List<Fantasquadra> getFantasquadreAffrontabili(Fantasquadra fantasquadra, int nCasaOTrasferta) {
		List<Fantasquadra> fantasquadreAffrontabili = new ArrayList<>();
		List<Long> idFantasquadreAffrontate = new ArrayList<>();
		
		Iterable<Partita> partiteGiocate;
		if (nCasaOTrasferta == 1)
			partiteGiocate = pRepository.findByFantasquadra1(fantasquadra);	
		else
			partiteGiocate = pRepository.findByFantasquadra2(fantasquadra);	

		if (partiteGiocate.spliterator().getExactSizeIfKnown() > 0) {
			for (Partita p: partiteGiocate)
				if (nCasaOTrasferta == 1)
					idFantasquadreAffrontate.add(p.getFantasquadra2().getId());
				else
					idFantasquadreAffrontate.add(p.getFantasquadra1().getId());
			fantasquadreAffrontabili = fsRepository.findByIdNotIn(idFantasquadreAffrontate);
		} else
			for (Fantasquadra f: fsRepository.findAll())
				fantasquadreAffrontabili.add(f);
			
		if (fantasquadreAffrontabili.contains(fantasquadra))
			fantasquadreAffrontabili.remove(fantasquadra);
		
		return fantasquadreAffrontabili;
	}
	
}